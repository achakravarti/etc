
;; Setup up package managers
;; We primarily use USE-PACKAGE
;; However, we require PACKAGE to first install USE-PACKAGE
;; We need to get the package lists from MELPA
;; We also update package lists if it is empty (as in new install)

(require 'package)
(add-to-list 'package-archives
			 '("melpa" . "https://melpa.org/packages/"))

;(when (not package-archive-contents)
 ;(package-refresh-contents))

;(package-refresh-contents)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; We want pretty fonts so we use ALL-THE-ICONS. For the
;; first time we also need to run ALL-THE-ICONS-INSTALL-FONTS
;; to ~/.local/share/fonts

(use-package all-the-icons
  :ensure t)
;;(all-the-icons-install-fonts)

;; Load PROJECTILE for project management

(use-package projectile
  :ensure t
  :config
  (projectile-mode 1))

;; Load the DASHBOARD package to show a pretty dashboard
;; on startup. Emacsclient shows the scratch buffer by
;; default, so we need to specify that it is to open the
;; dashboard instead on startup.

(use-package dashboard
  :ensure t
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Welcome to Emacs!")
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents . 5)
			  (agenda . 5)
			  (bookmarks . 5)
			  (projects . 5)
			  (registers . 5)))
  :config
  (dashboard-setup-startup-hook))

(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

;; The EVIL package allows vim keybindings, which is much
;; easier to use than then native Emacs ones. So let's go
;; ahead and install it with USE-PACKAGE. We also install
;; EVIL-COLLECTION to expand EVIL bindings to other areas
;; of emacs, namely the dashboard, directory and buffer
;; views
;; We disable C-i to restore TAB for org-mode, see
;; https://jeffkreeftmeijer.com/emacs-evil-org-tab/

(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode))

(use-package evil-collection
	     :ensure t
	     :after evil
	     :config
	     (setq evil-collection-mode-list '(dashboard dired ibuffer magit
					       mu4e))
	     (evil-collection-init))

(use-package evil-nerd-commenter
  :ensure t
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

;; The GENERAL package allows us to easily set keybindings, especially with SPC

(use-package general
  :ensure t
  :config (general-evil-setup t))

;; The FEATURE-MODE package provides a Gherkin major mode
;; https://github.com/michaelklishin/cucumber.el

(use-package feature-mode
  :ensure t
  :config
  (require 'feature-mode))


;; Prettify

(use-package doom-themes
  :ensure t)

(setq doom-themes-enable-bold t
      doom-themes-enable-italic t)

(load-theme 'doom-nord t)

(use-package doom-modeline
  :ensure t
  :config
  (doom-modeline-mode 1))

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(global-display-line-numbers-mode 1)
(global-visual-line-mode t)

;; Set fonts

(set-face-attribute 'default nil
		    :font " Fira Code Nerd Font"
		    :height 80
		    :weight 'light)

(set-face-attribute 'variable-pitch nil
		    :font "Fira Code Nerd Font"
		    :height 80)

(set-face-attribute 'fixed-pitch nil
		    :font "Fira Code Nerd Font"
		    :height 80)

;; Fill column at 80 and show fill column indicator
(setq-default fill-column 80)
(global-display-fill-column-indicator-mode t)

; does this work?
(setq global-prettify-symbols-mode t)


;; Set indentation for C code
(setq c-default-style "k&r"
      c-basic-offset 8
      tab-width 4
      indent-tabs-mode t)


;; Install autocompletion framework. We're choosing to try
;; VERTICO & friends

(use-package vertico
  :ensure t
  :bind (:map vertico-map
	      ("C-x" . vertico-exit)
	      ("C-j" . vertico-next)
	      ("C-k" . vertico-previous))
  :custom
  (vertico cycle t)
  :init
  (vertico-mode))

(use-package savehist
  :ensure t
  :init
  (savehist-mode))

(use-package marginalia
  :after vertico
  :ensure t
  :custom
  (marginalia-annotators '(margninalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))


;; Configure which key

(use-package which-key
  :ensure t
  :init
  (setq which-key-side-window-location 'bottom
	which-key-sort-order #'which-key-key-order-alpha
	which-key-sort-uppercase-first nil
	which-key-add-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 5
	which-key-side-window-slot -10
	which-key-side-window-max-height 0.25
	which-key-idle-delay 0.8
	which-key-max-description-length 100
	which-key-allow-imprecise-window-fit t
	which-key-separator " → " ))
(which-key-mode)

;; File/directory browsing

(use-package all-the-icons-dired
  :ensure t)

(nvmap :keymaps 'override :prefix "SPC"
  "SPC" '(execute-extended-command :which-key "Better M-x")
  "p" '(package-list-packages :which-key "Update packages")
  "d" '(dired :which-key "Open directory editor")
  "f" '(find-file :which-key "Find files"))

;; treemacs and winum
;; https://www.youtube.com/watch?v=M5cZNtCeJfs

(use-package treemacs
  :ensure t
  :config
  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode 'always)
  (treemacs-hide-gitignored-files-mode t)
  (treemacs-git-mode 'simple))

(use-package treemacs-evil
  :after treemacs
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-all-the-icons
  :after treemacs
  :ensure t
  :config
  (treemacs-load-all-the-icons-with-workaround-font "Fira Code Nerd Font"))

(use-package lsp-treemacs
  :ensure t
  :after (lsp-mode treemacs))

  

(use-package winum
  :ensure t
  :config
  (global-set-key (kbd "M-0") 'treemacs-select-window)
  (global-set-key (kbd "M-1") 'winum-select-window-1)
  (global-set-key (kbd "M-2") 'winum-select-window-2)
  (global-set-key (kbd "M-3") 'winum-select-window-3)
  ;(nvmap :keymaps 'override :prefix "SPC"
    ;"0" '(treemacs-select-window :which-key "Select Treemacs window")
    ;"1" '(winum-select-window-1 :which-key "Select window 1")
    ;"2" '(winum-select-window-2 :which-key "Select window 2")
    ;"3" '(winum-select-window-3 :which-key "Select window 3"))
  (winum-mode))

;; Install MAGIT package

(use-package magit
  :ensure t
  :config
  (nvmap :keymaps 'override :prefix "SPC"
    "g" '(magit-status :which-key "Run Magit status")))


;; Instal DIFF-HL package; we put the diff marker in the margin so that it
;; does not conflict with the flycheck markers on the fringe. We add the magit
;; hooks also, though I don't know what they are used for. Need to also consider
;; moving the magit hooks to the magit :hook section.

(use-package diff-hl
  :ensure t
  :after magit
  :config
  (global-diff-hl-mode)
  (diff-hl-margin-mode)
  (diff-hl-flydiff-mode))
(add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

;; Configure org-mode and prettify it

(use-package org
  :ensure t
  :init
  (setq org-startup-indented t)
  (setq org-startup-folded t)
  (setq org-directory "~/var/org"
	org-default-notes-file (expand-file-name "notes.org" org-directory)
	org-ellipsis " ▼ "
	org-log-done 'time)
  (setq org-startup-with-inline-images t)
  (setq org-display-remote-inline-images 'download)
  (setq org-src-preserve-indentation t
	org-src-tab-acts-natively t
	org-edit-src-content-indentation 2))

;; Activate org babel
;; https://orgmode.org/worg/org-contrib/babel/languages/index.html

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (shell . t)))

;; Enable htmlize; required by ReadTheOrg
(use-package htmlize
  :ensure t)

;; Ensure org-contact is enabled and configured
;; https://www.reddit.com/r/emacs/comments/8toivy/tip_how_to_manage_your_contacts_with_orgcontacts/

(use-package org-contacts
  :ensure t
  :after org
  :custom
  (org-contacts-files '("~/var/org/contacts.org")))

;; The simple HTTPd package helps run a local HTTP server for previewing the
;; locally developed web pages. Run with (httpd-serve-directory) to view the
;; webpage on localhost:8080

(use-package simple-httpd
  :ensure t)


;; Yasnippet; needed by lsp; need to study more
(use-package yasnippet-snippets
  :ensure t)

(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :config
    (yas-reload-all))

;; Install LSP mode along with COMPANY for autocompletion
;; LSP is the back-end for COMPANY
;; We need to take care to ensure that COMPANY restricts
;; its scope to the current major mode

(use-package lsp-mode
  :ensure t
  :hook
  ((sh-mode . (lambda ()
		(company-mode)
		(yas-minor-mode)
		(lsp-deferred)))
   (latex-mode . (lambda ()
		   (company-mode)
		   (yas-minor-mode)
		   (lsp-deferred)))
   (html-mode . (lambda ()
		  (company-mode)
		  (yas-minor-mode)
		  (lsp-deferred)))
   (css-mode . (lambda ()
		  (company-mode)
		  (yas-minor-mode)
		  (lsp-deferred)))
   (python-mode . (lambda ()
		    (company-mode)
		    (yas-minor-mode)
		    (lsp-deferred)))
   (vue-mode . (lambda ()
		 (company-mode)
		 (yas-minor-mode)
		 (lsp-deferred)))
   (c-mode . (lambda ()
	       (company-mode)
	       (yas-minor-mode) ; remove if diminish installed?
	       (lsp-deferred))))
  :config
  (lsp-enable-which-key-integration t))

;; (add-hook 'c-mode-hook 'lsp)
;; (add-hook 'c-mode-hook 'company-mode)
;; (add-hook 'c-mode-hook 'yas-minor-mode)

(use-package lsp-ui
  :ensure t
  :after lsp-mode
  :hook (lsp-mode . lsp-ui-mode)
  :config
  (setq lsp-ui-doc-position 'bottom)
  (setq lsp-enable-symbol-highlighting t)
  (setq lsp-ui-sideline-show-diagnostics t))

(use-package lsp-pyright
  :ensure t
  :hook
  (python-mode . (lambda ()
		   (require 'lsp-pyright)
		   (lsp-deferred))))

;; Enable vue-mode; typescript package seems to be required too.
;; NOT WORKING AS EXPECTED; ONLY GETTING SYNTAX HIGHLIGHTING

(use-package typescript-mode
  :ensure t)

(use-package vue-mode
  :ensure t)
	       
(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode))

(use-package company
  :ensure t
  ;; :hook
  ;; ((emacs-lisp-mode . (lambda () (setq-local company-backends '(company-elisp))))
  ;;  (c-mode . (lambda () (setq-local company-backends '(company-clang))))
  ;;  (c++-mode . (lambda () (setq-local company-backends '(company-clang))))
  ;;  (emacs-lisp-mode . company-mode)
  ;;  (c-mode . company-mode)
  ;;  (c++-mode . company-mode))
  :config
  (company-tng-mode)
  (setq company-idle-delay 0.1
	company-minimum-prefix-length 1
	company-selection-wrap-around t
	company-tooltip-align-annotations t))

;; prettify company with icons
;; https://github.com/TheBB/dotemacs/blob/master/init.el#L527-L570
(use-package company-box
  :ensure t
  :diminish company-box-mode
  :hook (company-mode . company-box-mode)
  :init
  (setq company-box-icons-alist 'company-box-icons-all-the-icons)
  :config
  (require 'all-the-icons)
  (setf (alist-get 'min-height company-box-frame-parameters) 6)
  (setq company-box-icons-alist 'company-box-icons-all-the-icons
        company-box-backends-colors nil
        company-box-icons-all-the-icons
        `((Unknown       . ,(all-the-icons-material "find_in_page"             :face 'all-the-icons-purple))
          (Text          . ,(all-the-icons-material "text_fields"              :face 'all-the-icons-green))
          (Method        . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
          (Function      . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
          (Constructor   . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
          (Field         . ,(all-the-icons-material "functions"                :face 'all-the-icons-red))
          (Variable      . ,(all-the-icons-material "adjust"                   :face 'all-the-icons-blue))
          (Class         . ,(all-the-icons-material "class"                    :face 'all-the-icons-red))
          (Interface     . ,(all-the-icons-material "settings_input_component" :face 'all-the-icons-red))
          (Module        . ,(all-the-icons-material "view_module"              :face 'all-the-icons-red))
          (Property      . ,(all-the-icons-material "settings"                 :face 'all-the-icons-red))
          (Unit          . ,(all-the-icons-material "straighten"               :face 'all-the-icons-red))
          (Value         . ,(all-the-icons-material "filter_1"                 :face 'all-the-icons-red))
          (Enum          . ,(all-the-icons-material "plus_one"                 :face 'all-the-icons-red))
          (Keyword       . ,(all-the-icons-material "filter_center_focus"      :face 'all-the-icons-red))
          (Snippet       . ,(all-the-icons-material "short_text"               :face 'all-the-icons-red))
          (Color         . ,(all-the-icons-material "color_lens"               :face 'all-the-icons-red))
          (File          . ,(all-the-icons-material "insert_drive_file"        :face 'all-the-icons-red))
          (Reference     . ,(all-the-icons-material "collections_bookmark"     :face 'all-the-icons-red))
          (Folder        . ,(all-the-icons-material "folder"                   :face 'all-the-icons-red))
          (EnumMember    . ,(all-the-icons-material "people"                   :face 'all-the-icons-red))
          (Constant      . ,(all-the-icons-material "pause_circle_filled"      :face 'all-the-icons-red))
          (Struct        . ,(all-the-icons-material "streetview"               :face 'all-the-icons-red))
          (Event         . ,(all-the-icons-material "event"                    :face 'all-the-icons-red))
          (Operator      . ,(all-the-icons-material "control_point"            :face 'all-the-icons-red))
          (TypeParameter . ,(all-the-icons-material "class"                    :face 'all-the-icons-red))
          (Template      . ,(all-the-icons-material "short_text"               :face 'all-the-icons-green)))))

;; company support for shell scripts
;; (use-package company-shell
;;   :ensure t
;;   :after company
;;   :hook
;;   ((sh-mode . (lambda () (setq-local company-backends '(company-shell))))
;;    (sh-mode . company-mode)))



;;(use-package org-bullets
  ;;:ensure t)
;;(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))


(use-package org-superstar
  :after org
  :ensure t
  :hook (org-mode . org-superstar-mode)
  :custom
  (org-superstar-headline-bullets-list '("⁖" "◉" "○" "✸" "✿")))
  

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/var/org/roam")
  :config
  (setq org-roam-node-display-template
	(concat "${title} "
		(propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode))


;; Setup mu4e

(use-package mu4e
  :ensure nil
  ;; :load-path "/usr/share/emacs/site-lisp/mu4e/"
  ;; :defer 20 ; Wait until 20 seconds after startup
  :config

  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Use fancy characters
  ;; https://mu-discuss.narkive.com/0A8jgd4g/fyi-nicer-threading-characters
  (setq mu4e-use-fancy-chars t
    mu4e-headers-thread-child-prefix '("├>" . "├▶ ")
    mu4e-headers-thread-last-child-prefix '("└>" . "└▶ ")
    mu4e-headers-thread-connection-prefix '("│" . "│ ")
    mu4e-headers-thread-orphan-prefix '("┬>" . "┬▶ ")
    mu4e-headers-thread-single-orphan-prefix '("─>" . "─▶ ")
    mu4e-headers-unread-mark '("u" . "○")
    mu4e-headers-flagged-mark '("F" . "⚑")
    mu4e-headers-attach-mark '("a" . "📎")
    mu4e-headers-seen-mark '("S" . " ")
    mu4e-headers-replied-mark '("R" . "↵")
    mu4e-headers-passed-mark '("P" . "⇉")
    mu4e-headers-signed-mark '("s" . "✍")
    mu4e-headers-trashed-mark '("T" . "×"))

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "/home/abhishek/var/mail")
  (setq message-send-mail-function 'smtpmail-send-it)

  ;; Set mail signature
  ;; https://gist.github.com/lehoff/ab14ec0441e1c511a738
  (setq message-signature-file "~/.emacs.d/sig")

  (setq mu4e-contexts
        (list
	 ;; Taranjali account
         (make-mu4e-context
          :name "Taranjali"
          :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p "/taranjali" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "abhishek@taranjali.org")
                  (user-full-name    . "Abhishek Chakravarti")
		  (smtpmail-smtp-user . "abhishek@taranjali.org")
		  (smtpmail-smtp-server . "mail.taranjali.org")
		  (smtpmail-smtp-service . 587)
		  (smtpmail-smtp-stream-type . starttls)
                  (mu4e-drafts-folder  . "/taranjali/Drafts")
                  (mu4e-sent-folder  . "/taranjali/Sent")
                  (mu4e-trash-folder  . "/taranjali/Trash"))))))

;; Add diff colouring to emailed patch messages
;; https://github.com/seanfarley/message-view-patch
(use-package message-view-patch
  :ensure t)
(add-hook 'gnus-part-display-hook 'message-view-patch-highlight)


(nvmap :keymaps 'override :prefix "SPC"
  "o c" '(org-capture :which-key "Capture note")
  "o r c" '(org-roam-capture :which-key "Capture node")
  "o r d" '(org-roam-dailies-capture-today :which-key "Capture daily node")
  "o r f" '(org-roam-node-find :which-key "Find node")
  "o r s" '(org-roam-db-sync :which-key "Sync database"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("70b596389eac21ab7f6f7eb1cf60f8e60ad7c34ead1f0244a577b1810e87e58c" "7e068da4ba88162324d9773ec066d93c447c76e9f4ae711ddd0c5d3863489c52" "1a1ac598737d0fcdc4dfab3af3d6f46ab2d5048b8e72bc22f50271fd6d393a00" default))
 '(package-selected-packages
   '(simple-httpd evil-nerd-commentor: evil-nerd-commenter: treemacs-all-the-icons treemacs-evil treemacs company-box company-shell feature-mode flycheck lsp-ui company lsp-mode org-superstar all-the-icons-dired org-bullets projectile all-the-icons which-key doom-themes marginalia vertico org-roam general use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
