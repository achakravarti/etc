#!/bin/sh

# Ensure that we're running either FreeBSD or Arch Linux
check_os() {
    kernel=$(uname -s | tr '[:upper:]' '[:lower:]')
    emsg='ERROR: only FreeBSD and Arch Linux supported'

    [ "${kernel}" != freebsd ]      \
	&& [ "${kernel}" != linux ] \
	&& echo "${emsg}" ; exit 1

    if [ "${kernel}" = linux ]
    then
	[ -f /etc/os-release ] || echo "${emsg}" ; exit 1

        DISTRO=$(grep 'NAME' /etc/os-release \
            | head -n 1                      \
            | cut -d '=' -f 2                \
            | tr -d '"')

	[ "${DISTRO}" != arch ] || echo "${emsg}" ; exit 1
    fi
}


# Installs Emacs and related tools
install_emacs() {
    case "${DISTRO}" in
	arch)
	    sudo pacman -Syu --noconfirm
	    sudo pacman -S emacs --noconfirm
	    sudo pacman -S nodejs npm --noconfirm
	    sudo pacman -S python python-pip python-virtualenv pyright --noconfirm
	    sudo pacman -S clang --noconfirm
	    yay -S bash-language-server vscode-langservers-extracted --noconfirm
	;;
	freebsd)
	    echo 'ERROR: not implemented for FreeBSD' ; return 1
	    ;;
	*)
	    echo 'ERROR: unsupported OS!' ; return 1
	    ;;
    esac
}


check_os
install_emacs
